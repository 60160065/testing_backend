const dbHandler = require('./db-handler')
const Announce = require('../models/Announce')

beforeAll(async () => {
  await dbHandler.connect()
})
afterEach(async () => {
  await dbHandler.clearDatabase()
})
afterAll(async () => {
  await dbHandler.closeDatabase()
})

const announceComplete = {
  topic: 'abcde',
  content: '0123456789'
}
const announceErrorTopicEmpty = {
  topic: '',
  content: '0123456789'
}
const announceErrorTopicEmptyAndContent9Alphabets = {
  topic: '',
  content: '012345678'
}
const announceErrorContentEmpty = {
  topic: 'abcde',
  content: ''
}
const announceErrorContentEmptyAndTopic4Alphabets = {
  topic: 'abcd',
  content: ''
}
const announceErrorTopicAndContentEmpty = {
  topic: '',
  content: ''
}
const announceErrorTopic4Alphabets = {
  topic: 'abcd',
  content: '0123456789'
}
const announceErrorContent9Alphabets = {
  topic: 'abcde',
  content: '012345678'
}
const announceErrorTopic4AlphabetsAndContent9Alphabets = {
  topic: 'abcd',
  content: '012345678'
}

describe('Announce', () => {
  it('สามารถเพิ่มประกาศได้และเนื้อหาประกาศ 10 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceComplete)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ หัวข้อ เป็น ช่องว่างและเนื้อหาประกาศเป็น 10 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorTopicEmpty)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ หัวข้อ เป็น ช่องว่างและเนื้อหาประกาศเป็น 9 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorTopicEmptyAndContent9Alphabets)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ เนื้อหาประกาศ เป็น ช่องว่างและหัวข้อเป็น 5 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorContentEmpty)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ เนื้อหาประกาศ เป็น ช่องว่างและหัวข้อเป็น 4 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorContentEmptyAndTopic4Alphabets)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ หัวข้อ และ เนื้อหาประกาศ เป็น ช่องว่าง', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorTopicAndContentEmpty)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ หัวข้อ เป็น 4 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorTopic4Alphabets)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ เนื้อหาประกาศ เป็น 9 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorContent9Alphabets)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศได้เพราะ หัวข้อ เป็น 4 ตัว และ เนื้อหาประกาศ เป็น 9 ตัว', async () => {
    let error = null
    try {
      const announce = new Announce(announceErrorTopic4AlphabetsAndContent9Alphabets)
      await announce.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มประกาศที่ซ้ำกันได้', async () => {
    let error = null
    try {
      const announce1 = new Announce(announceComplete)
      await announce1.save()
      const announce2 = new Announce(announceComplete)
      await announce2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
