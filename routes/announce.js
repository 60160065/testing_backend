const express = require('express')
const router = express.Router()
// const usersController = require('../controller/UserController')
const User = require('../models/Announce')

/* GET users listing. */
router.get('/', async (req, res, next) => {
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  try {
    const user = await User.findById(id)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.post('/', async (req, res, next) => {
  const payload = req.body
  const user = new User(payload)
  try {
    await user.save()
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.put('/', async (req, res, next) => {
  const payload = req.body
  try {
    const user = await User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  try {
    const user = await User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
module.exports = router
