const express = require('express')
const router = express.Router()
// const usersController = require('../controller/UserController')
const User = require('../models/Announce')

/* GET users listing. */
router.get('/', async (req, res, next) => {
  // res.json(usersController.getUsers())
  // User.find({}).exec(function (err, users) {
  //   if (err) {
  //     res.status(500).send()
  //   }
  //   res.json(users)
  // })
  // User.find({}).then(function (users) {
  //   res.json(users)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
  // Async Await
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  //   const { id } = req.params
  //   res.json(usersController.getUser(id))
  try {
    const user = await User.findById(id)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.post('/', async (req, res, next) => {
  const payload = req.body
  // res.json(usersController.addUser(payload))
  // User.create(payload).then(function(user) {
  //   res.json(user)
  // }).catch(function(err){
  //   res.status(500).send(err)
  // })
  // res.json(usersController.addUser(payload))
  const user = new User(payload)
  try {
    await user.save()
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.put('/', async (req, res, next) => {
  const payload = req.body
  // res.json(usersController.updateUser(payload))
  try {
    const user = await User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.deleteUser(id))
  try {
    const user = await User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
module.exports = router
