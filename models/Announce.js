const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  topic: {
    type: String,
    required: true,
    minlength: 5,
    unique: true
  },
  content: {
    type: String,
    required: true,
    minlength: 10
  },
  date: { type: Date, required: true, default: Date.now }
})

module.exports = mongoose.model('Announce', userSchema)
